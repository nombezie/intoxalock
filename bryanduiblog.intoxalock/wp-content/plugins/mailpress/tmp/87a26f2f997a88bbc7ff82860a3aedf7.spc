<?xml version="1.0" encoding="UTF-8"?>
<geoPlugin>
	<geoplugin_request>65.124.135.2</geoplugin_request>
	<geoplugin_status>200</geoplugin_status>
	<geoplugin_credit>Some of the returned data includes GeoLite data created by MaxMind, available from &lt;a href=\'http://www.maxmind.com\'&gt;http://www.maxmind.com&lt;/a&gt;.</geoplugin_credit>
	<geoplugin_city>West Des Moines</geoplugin_city>
	<geoplugin_region>IA</geoplugin_region>
	<geoplugin_areaCode>515</geoplugin_areaCode>
	<geoplugin_dmaCode>679</geoplugin_dmaCode>
	<geoplugin_countryCode>US</geoplugin_countryCode>
	<geoplugin_countryName>United States</geoplugin_countryName>
	<geoplugin_continentCode>NA</geoplugin_continentCode>
	<geoplugin_latitude>41.563499</geoplugin_latitude>
	<geoplugin_longitude>-93.784203</geoplugin_longitude>
	<geoplugin_regionCode>IA</geoplugin_regionCode>
	<geoplugin_regionName>Iowa</geoplugin_regionName>
	<geoplugin_currencyCode>USD</geoplugin_currencyCode>
	<geoplugin_currencySymbol>&amp;#36;</geoplugin_currencySymbol>
	<geoplugin_currencySymbol_UTF8>$</geoplugin_currencySymbol_UTF8>
	<geoplugin_currencyConverter>1</geoplugin_currencyConverter>
</geoPlugin>
