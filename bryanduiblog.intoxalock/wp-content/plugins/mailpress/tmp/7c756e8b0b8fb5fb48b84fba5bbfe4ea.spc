<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<site>http://ip-json.rhcloud.com</site>
	<city>Pagosa Springs</city>
	<region_name>Colorado</region_name>
	<region>CO</region>
	<area_code>970</area_code>
	<time_zone>America/Denver</time_zone>
	<longitude>-107.080200195</longitude>
	<metro_code>773</metro_code>
	<country_code3>USA</country_code3>
	<latitude>37.3442001343</latitude>
	<postal_code>81147</postal_code>
	<dma_code>773</dma_code>
	<country_code>US</country_code>
	<country_name>United States</country_name>
	<q>72.161.117.170</q>
</Response>
