			<hr />
			<footer>
				<nav>
					<?php wp_nav_menu( array(  'theme_location' => 'footer', 'menu_class' => 'nav nav-pills', 'fallback_cb' => false ) ); ?>
				</nav>
			</footer>
		</div> <!-- closes .container -->
		<?php wp_footer(); ?>
		<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//www.intoxalock.com/analytics/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 5]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//www.intoxalock.com/analytics/piwik.php?idsite=5" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
	</body>
</html>