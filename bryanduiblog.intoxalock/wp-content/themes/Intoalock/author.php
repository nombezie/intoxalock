<?php get_header(); ?>
<div class="row">
	<div class="span9">
		<?php if (have_posts()) : the_post();
		// for more information on author meta values, refer to http://codex.wordpress.org/Function_Reference/the_author_meta
			$id = get_the_author_meta('ID');
			$fname = get_the_author_meta('user_firstname');
			$lname = get_the_author_meta('user_lastname');
			$bio = get_the_author_meta('description');
			$jabber = get_the_author_meta('jabber');
			$aim = get_the_author_meta('aim');
			$yim = get_the_author_meta('yim');
			$url = get_the_author_meta('user_url');
		?>
		<div class="media">
			<div class="pull-left media-object">
				<?php echo get_avatar( $id, 128 ); ?>
			</div>
			<div class="media-body">
				<div class="hero-unit">
					<h1>
						<?php if ( $fname ) {
							echo $fname . '&nbsp;' . $lname;
						} else {
							the_author_meta('user_nicename');
						} ?>
					</h1>
					<?php if ( $bio )
						echo wpautop($bio); ?>
					<div class="author-info">
						<?php if ( $aim )
							printf( '<strong>%s</strong>' . $aim . '<br />', __( 'AOL Instant Messenger: ', 'pluralsight' ) );
						if ( $yim )
							printf( '<strong>%s</strong>' . $yim . '<br />', __( 'Yahoo! Instant Messenger: ', 'pluralsight' ) );
						if ( $jabber )
							printf( '<strong>%s</strong>' . $jabber . '<br />', __( 'Google Chat/Jabber: ', 'pluralsight' ) );
						if ( $url )
							printf( '<strong>%s</strong> <a href="' . $url . '" target="_blank">' . $url . '</a>', __( 'Website: ', 'pluralsight' ) );
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
			endif;
			wp_reset_query();
			if (have_posts()) : while (have_posts()) : the_post();
			$post_format = get_post_format();
			get_template_part('post',$post_format);
		?>
		<?php endwhile; ?>
		<ul class="nav nav-pills">
			<li class="alignleft"><?php previous_posts_link('&laquo; Previous Entries') ?></li>
			<li class="alignright"><?php next_posts_link('Next Entries &raquo;','') ?></li>
		</ul>
		<?php endif; ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>