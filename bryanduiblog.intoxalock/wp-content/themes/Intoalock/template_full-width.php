<?php
	/*
		Template Name: Full Width
	*/
?>
<?php get_header(); ?>
<div class="row">
	<div class="span12">
		<?php if (have_posts()) : while (have_posts()) : the_post();
			get_template_part('parts/post','page');
		?>
		<?php endwhile; endif; ?>
	</div>
</div>
<?php get_footer(); ?>