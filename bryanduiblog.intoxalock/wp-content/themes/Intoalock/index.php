<?php get_header(); ?>
<div class="row">
	<div class="span9" style="font-size: 12px;font-family: Arial;color:#000">
        <p style="margin-left: 20px; font-size: 30px;font-weight: bold;color: #9e9e9d;margin-top: 50px">Latest News</p>
     <?php $the_query = new WP_Query( 'showposts=1' ); ?>
     <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
     <p style="margin-left: 20px; font-size: 12px;"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>
    
      <?php endwhile;?>
		<?php if (have_posts()) : while (have_posts()) : the_post();
			$post_format = get_post_format();
			get_template_part('parts/post',$post_format);
		?>
		<?php endwhile; ?>
		<ul class="nav nav-pills">
			<li class="alignleft"><?php previous_posts_link('&laquo; Previous Entries') ?></li>
			<li class="alignright"><?php next_posts_link('Next Entries &raquo;','') ?></li>
		</ul>
		<?php endif; ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>