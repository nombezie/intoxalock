<?php get_header(); ?>
<div class="row">
	<div class="span9">
		<section class="entry">
			<h1 class="page-header"><?php _e( 'Oops...<br /><small>The page you were looking for could not be found.</small>', 'pluralsight' ); ?></h1>
			<?php get_search_form(); ?>
			<div class="span3 first">
				<h3><?php _e( 'Post archives by Month', 'pluralsight' ); ?></h3>
				<ul class="nav nav-tabs nav-stacked">
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</div>
			<div class="span3">
				<h3><?php _e( 'Post Archives by Category', 'pluralsight' ); ?></h3>
				<ul class="nav nav-tabs nav-stacked">
					<?php wp_list_categories( 'title_li=&hierarchical=0' ); ?>
				</ul>
			</div>
			<div class="span3">
				<h3><?php _e( 'Links', 'pluralsight' ); ?></h3>
				<ul class="nav nav-tabs nav-stacked">
					<?php wp_list_bookmarks( 'title_li=&categorize=0' ); ?>
				</ul>
			</div>
		</section>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>