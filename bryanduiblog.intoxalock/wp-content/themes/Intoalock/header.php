<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="container">

            <?php if ( get_header_image() ) { ?>
                <header class="row header-image">
                    <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" />
            <?php } else { ?>
                <header class="row">
            <?php } ?>


                <hgroup class="span12">
                    <nav role="navigation" class="navbar" >
                        <?php wp_nav_menu( array( 'container_class' => 'navbar-inner nav-collapse', 'menu_class' => 'nav', 'theme_location' => 'header', 'fallback_cb' => false, 'walker' => new pluralsight_walker_nav_menu ) ); ?>
                    </nav>
                    
        </hgroup>

                
            </header>