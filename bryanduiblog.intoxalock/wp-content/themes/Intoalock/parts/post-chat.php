<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<section class="entry">
		<p class="lead"><?php the_title(); ?></p>
		<?php the_content(); ?>
		<section class="postmeta pull-right">
			<small><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></small>
		</section>
	</section>
</article>