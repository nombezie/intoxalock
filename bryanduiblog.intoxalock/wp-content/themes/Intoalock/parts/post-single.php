<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<h1 class="page-header"><?php the_title(); ?></h1>
	<section class="entry">
		<?php the_content(); ?>
		<section class="postmeta">
		<?php
			$time = '<time datetime=' . get_the_time('Y-m-d') . '>' . get_the_time('j F Y') . '</time>';
			$categories = get_the_category_list( __(', ', 'pluralsight') );
			$tags = get_the_tag_list( __('and tagged ', 'pluralsight'),', ' );
			$author_url = get_author_posts_url(get_the_author_meta( 'ID' ));
			$author = '<a href="' . $author_url . '">' . get_the_author_meta( 'display_name' ) . '</a>';
			$postmeta = __('Posted in %1$s on %2$s by %3$s %4$s', 'pluralsight');
			printf( $postmeta, $categories, $time, $author, $tags );
		?>
		</section>
		<nav><?php wp_link_pages(); ?></nav>
	</section>
</article>