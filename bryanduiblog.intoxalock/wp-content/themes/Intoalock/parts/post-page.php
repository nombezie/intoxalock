<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<h1 class="page-header"><?php the_title(); ?></h1>
	<section class="entry">
		<?php the_content(); ?>
		<nav><?php wp_link_pages(); ?></nav>
	</section>
</article>