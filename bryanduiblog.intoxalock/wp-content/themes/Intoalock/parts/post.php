<article <?php post_class('media'); ?> id="post-<?php the_ID(); ?>">
	<?php if ( has_post_thumbnail() ) { ?>
		<a class="pull-left" href="<?php the_permalink(); ?>">
				<?php $atts = array(
					'class' => 'media-object'
				); ?>
				<?php the_post_thumbnail( 'thumbnail', $atts ); ?>
		</a>
	<?php } ?>
	<div class="media-body">
<section class="entry" style="margin-left: 20px">
<div class="media-heading" style="text-decoration: none;font-size: 17px; font-weight: bold"><a href="<?php the_permalink(); ?>" style="color: #f8941e"><?php the_title(); ?></a></div>
<div style="color: #9e9e9d;font-family: Arial">
 <?php		
$time = '<time datetime=' . get_the_time('Y-m-d') . '>' . get_the_time('F j, Y') . '</time>';													
printf( $time);
?>
</div>
			<?php the_excerpt(); ?>
		</section>
	</div>
	<nav><?php wp_link_pages(); ?></nav>
      <p style="text-decoration: underline;margin-left: 20px"><a href="<?php get_comment($comment, $args, $depth); ?> ">
    COMMENTS
</a></p>
</article>
