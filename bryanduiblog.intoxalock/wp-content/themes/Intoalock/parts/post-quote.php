<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<section class="entry">
		<p class="lead"><?php the_title(); ?></p>
			<?php the_content(); ?>
	</section>
</article>