<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<h1 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
	<section class="entry">
		<?php the_content(); ?>
		<nav><?php wp_link_pages(); ?></nav>
	</section>
</article>