<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<section class="entry thumbnails">
		<div class="thumbnail">
			<?php the_content(); ?>
			<h3 class="offset1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>
	</section>
</article>