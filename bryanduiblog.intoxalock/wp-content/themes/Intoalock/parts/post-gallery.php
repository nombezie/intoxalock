<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<section class="entry">
<div style="font-size: 17px;font-family: Arial;font-weight: bold;color: #f8941e"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
		<?php the_content(); ?>
		<nav><?php wp_link_pages(); ?></nav>
	</section>
</article>