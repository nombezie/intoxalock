<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<script src="http://www.intoxalockdev.com/Portals/_default/Skins/Intoxalock/js/jquery.maskedinput.js?" type="text/javascript"></script>
<?php 

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	define('NEWLINE', "<br />\n");

// SOAP client
$sampleData->FirstName = $_POST["qsName"];
$sampleData->PhoneNumber = $_POST["qsPhone"];
$sampleData->ZipCode = $_POST["qsZip"];
$sampleData->EmailAddress = $_POST["qsEmail"];
$sampleData->SourcePage = "http://duiblog.intoxalock.com/";
$sampleData->SuggestedCallbackDateTime = date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000));
$parameters->request = $sampleData;


$soap = new SoapClient("https://webservices.intoxalockdev.com:9002/IntoxalockService.svc?singleWsdl",
   array('soap_version' => 'SOAP_1_1',

         '$location'=>'https://webservices.intoxalockdev.com:9002/IntoxalockService.svc'));

$obj = $soap->CreateQuickStartContact($parameters);
$number = $obj->CreateQuickStartContactResult->ContactID;
$dup = $obj->CreateQuickStartContactResult->DuplicateContact;
$soap = null;
//if($dup !='')
//{
	header('Location: http://intoxalock.com/');
//}
 // SOAP call

}
?>

<style>
  .install-Callout {
  background-color: rgb(230, 230, 230);
  padding: 10px;
  text-align: center;
  margin-bottom: 15px;
  font-family: Arial, sans-serif;

  }

  .install-Callout span {
  font-size: 18px;
  font-weight: bold;

  }

  .install-Callout input {
  width: 80%;
  margin: 10px 0px 0px 0px;
  padding: 4px;
  border-style: none;
  font-size: 9pt;
  color: gray;
  }

  input.btnImage {
  background-image: url("/wp-content/themes/twentyten/images/get_started.png");
  background-repeat: no-repeat;
  background-position: center center;
  height: 35px;
  background-color: transparent;
  cursor: pointer;
  font-size:0px;
  line-height:0;
  }

</style>

<script type="text/javascript">
         function Validations() {
            var validateFaled = 0;
            if ($("#qsName").val() == "Name") {
                $("#qsName").css('border-style', 'solid');
                $("#qsName").css('border-color', 'red');
                validateFaled = 1;
            }
            else {
                $("#qsName").css('border-style', 'none');
                
            }

            if ($("#qsPhone").val() == "Phone") {
                $("#qsPhone").css('border-style', 'solid');
                $("#qsPhone").css('border-color', 'red');
                validateFaled = 2;
                
            }
            else {
                $("#qsPhone").css('border-style', 'none');
               
            }
            if ($("#qsZip").val() == "Zip code") {
                $("#qsZip").css('border-style', 'solid');
                $("#qsZip").css('border-color', 'red');
                validateFaled = 2;

            }
            else {
                $("#qsZip").css('border-style', 'none');

            }
            if ($("#qsEmail").val() == "Email" || validateEmail($("#qsEmail").val()) == false) {
                $("#qsEmail").css('border-style', 'solid');
                $("#qsEmail").css('border-color', 'red');
                validateFaled = 2;

            }
            else {
                $("#qsEmail").css('border-style', 'none');

            }
            if ($("#qsEmail").val() == "Email" || $("#qsZip").val() == "Zip code" || $("#qsPhone").val() == "Phone" || $("#qsName").val() == "Name" || validateEmail($("#qsEmail").val()) == false) {
                return false
            }
            else {
                return true;
            }
              
            


        }


    $(document).ready(function () {
      $("#qsPhone").mask("(999) 999-9999");
	  
	     $("#qsPhone").attr('value', "Phone");
            $('#qsPhone').focus(
           function () {
               if ($(this).val() == 'Phone') {
                   $(this).val('');
               }
           })
           .blur(function () {
               if ($(this).val() == '') {
                   $(this).val('Phone');
               }
           });
		   
		    //To check if you want to check phone number on KeyPress
            $("#qsPhone").attr('maxlength', '14');
            $("#qsZip").attr('maxlength', '5');
            $("#qsMZip").attr('maxlength', '5');

            //first name
            $('#qsName').keypress(

                   function (event) {
                       var keycode = event.which;
                       if ((event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
                           event.preventDefault();
                       }
                   });
            $('#qsPhone').keypress(

                   function (event) {
                       if (!event.which != 8 && isNaN(String.fromCharCode(event.which)) && !event.which == 9) {
                           event.preventDefault(); //stop character from entering input
                       }
                   });
            $('#qsZip').keypress(

                   function (event) {
                       if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                           event.preventDefault(); //stop character from entering input
                       }
                   });


            //Mobile
            $('#qsMName').keypress(

               function (event) {
                   if (!event.which == 8 && !isNaN(String.fromCharCode(event.which))) {
                       event.preventDefault(); //stop numbers from entering input
                   }
               });
            $('#qsMPhone').keypress(

                   function (event) {
                       if (!event.which != 8 && isNaN(String.fromCharCode(event.which)) && !event.which == 9) {
                           event.preventDefault(); //stop character from entering input
                       }
                   });
            $('#qsMZip').keypress(

                   function (event) {
                       if (!event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                           event.preventDefault(); //stop character from entering input
                       }
                   });

  
		 

    });


</script>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="xoxo" style="margin:0px; padding: 0px;">
						<li>
              <form action="index.php" method="POST">
                <div class="install-Callout">
                  <span>Ready to install?</span>

                  <div>
                    <input name="qsName" type="text" value="Name" id="qsName" class="textfield" onfocus="if (this.value==this.defaultValue) this.value = &#39;&#39;" onblur="if (this.value==&#39;&#39;) this.value = this.defaultValue" />
                  </div>
                  <div>
                    <input name="qsPhone" type="text" value="Phone" id="qsPhone" class="textfield" />
                  </div>
                  <div>
                    <input name="qsZip" type="text" value="Zip code" id="qsZip" class="textfield" onfocus="if (this.value==this.defaultValue) this.value = &#39;&#39;" onblur="if (this.value==&#39;&#39;) this.value = this.defaultValue" />
                  </div>
                  <div>
                    <input name="qsEmail" type="text" value="Email" id="qsEmail" class="textfield" onfocus="if (this.value==this.defaultValue) this.value = &#39;&#39;" onblur="if (this.value==&#39;&#39;) this.value = this.defaultValue" />
                  </div>

                  <input class="btnImage" onclick="return Validations();"  type="submit" name="submit"  />
                </div>
              </form>

			</li>

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
                       <div id="searchFom" class="mysearch" style="border:soild 1px black;">
			<li id="search" class="search-form widget_search">
				<?php get_search_form(); ?>
			</li>
                      </div>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</li>

			<li id="meta" class="widget-container" style="padding-bottom: 10px;">
				<h3 class="widget-title"><?php _e( 'Meta', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</li>

		<?php endif; // end primary widget area ?>
			</ul>
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
  
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>
