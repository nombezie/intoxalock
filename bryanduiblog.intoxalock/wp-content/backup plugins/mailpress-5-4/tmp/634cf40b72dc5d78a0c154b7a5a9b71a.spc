<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>107.77.70.16</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>NY</RegionCode>
	<RegionName>New York</RegionName>
	<City>New York</City>
	<ZipCode>10118</ZipCode>
	<TimeZone>America/New_York</TimeZone>
	<Latitude>40.714</Latitude>
	<Longitude>-74.006</Longitude>
	<MetroCode>501</MetroCode>
</Response>
