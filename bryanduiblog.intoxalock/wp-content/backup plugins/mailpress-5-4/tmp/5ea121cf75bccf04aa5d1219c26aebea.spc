<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>198.40.239.26</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>IA</RegionCode>
	<RegionName>Iowa</RegionName>
	<City>Boone</City>
	<ZipCode>50036</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>42.06</Latitude>
	<Longitude>-93.881</Longitude>
	<MetroCode>679</MetroCode>
</Response>
