<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>68.56.85.224</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>FL</RegionCode>
	<RegionName>Florida</RegionName>
	<City>Avon Park</City>
	<ZipCode>33825</ZipCode>
	<TimeZone>America/New_York</TimeZone>
	<Latitude>27.596</Latitude>
	<Longitude>-81.507</Longitude>
	<MetroCode>539</MetroCode>
</Response>
