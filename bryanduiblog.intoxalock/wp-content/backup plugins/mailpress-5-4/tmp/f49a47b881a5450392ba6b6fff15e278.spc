<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>50.139.191.210</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>VT</RegionCode>
	<RegionName>Vermont</RegionName>
	<City>Swanton</City>
	<ZipCode>05488</ZipCode>
	<TimeZone>America/New_York</TimeZone>
	<Latitude>44.907</Latitude>
	<Longitude>-73.144</Longitude>
	<MetroCode>523</MetroCode>
</Response>
