<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>66.87.145.178</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>MN</RegionCode>
	<RegionName>Minnesota</RegionName>
	<City>Minneapolis</City>
	<ZipCode>55454</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>44.971</Latitude>
	<Longitude>-93.25</Longitude>
	<MetroCode>613</MetroCode>
</Response>
