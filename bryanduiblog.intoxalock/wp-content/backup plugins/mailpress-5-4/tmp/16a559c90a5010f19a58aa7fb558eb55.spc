<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>199.187.215.81</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>OK</RegionCode>
	<RegionName>Oklahoma</RegionName>
	<City>Quapaw</City>
	<ZipCode>74363</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>36.946</Latitude>
	<Longitude>-94.682</Longitude>
	<MetroCode>603</MetroCode>
</Response>
