<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>66.97.144.2</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>TX</RegionCode>
	<RegionName>Texas</RegionName>
	<City>Dallas</City>
	<ZipCode>75201</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>32.783</Latitude>
	<Longitude>-96.807</Longitude>
	<MetroCode>623</MetroCode>
</Response>
