<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Ip>173.242.98.34</Ip>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>NY</RegionCode>
	<RegionName>New York</RegionName>
	<City>Plattsburgh</City>
	<ZipCode>12903</ZipCode>
	<Latitude>44.6834</Latitude>
	<Longitude>-73.4458</Longitude>
	<MetroCode>523</MetroCode>
	<AreaCode>518</AreaCode>
</Response>
