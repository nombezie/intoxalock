<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Ip>71.7.70.158</Ip>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>IA</RegionCode>
	<RegionName>Iowa</RegionName>
	<City>Orient</City>
	<ZipCode>50858</ZipCode>
	<Latitude>41.2152</Latitude>
	<Longitude>-94.4231</Longitude>
	<MetroCode>679</MetroCode>
	<AreaCode>641</AreaCode>
</Response>
