<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>65.124.135.2</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>IA</RegionCode>
	<RegionName>Iowa</RegionName>
	<City>West Des Moines</City>
	<ZipCode>50266</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>41.564</Latitude>
	<Longitude>-93.785</Longitude>
	<MetroCode>679</MetroCode>
</Response>
