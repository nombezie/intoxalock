<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>173.23.45.141</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>IL</RegionCode>
	<RegionName>Illinois</RegionName>
	<City>Silvis</City>
	<ZipCode>61282</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>41.512</Latitude>
	<Longitude>-90.416</Longitude>
	<MetroCode>682</MetroCode>
</Response>
