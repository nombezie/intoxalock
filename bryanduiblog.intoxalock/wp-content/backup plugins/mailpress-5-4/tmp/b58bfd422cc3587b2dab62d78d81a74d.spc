<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>24.240.48.46</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>WI</RegionCode>
	<RegionName>Wisconsin</RegionName>
	<City>Madison</City>
	<ZipCode>53719</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>43.029</Latitude>
	<Longitude>-89.519</Longitude>
	<MetroCode>669</MetroCode>
</Response>
