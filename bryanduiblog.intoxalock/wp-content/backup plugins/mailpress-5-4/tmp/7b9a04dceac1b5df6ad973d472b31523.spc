<?xml version="1.0" encoding="UTF-8"?>
<Response>
 <Ip>208.54.83.197</Ip>
 <CountryCode>US</CountryCode>
 <CountryName>United States</CountryName>
 <RegionCode>TX</RegionCode>
 <RegionName>Texas</RegionName>
 <City>Houston</City>
 <ZipCode></ZipCode>
 <Latitude>29.7633</Latitude>
 <Longitude>-95.3633</Longitude>
 <MetroCode>618</MetroCode>
 <AreaCode>713</AreaCode>
</Response>
