<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Ip>66.225.14.190</Ip>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>WA</RegionCode>
	<RegionName>Washington</RegionName>
	<City>Kennewick</City>
	<ZipCode>99336</ZipCode>
	<Latitude>46.2131</Latitude>
	<Longitude>-119.1682</Longitude>
	<MetroCode>810</MetroCode>
	<AreaCode>509</AreaCode>
</Response>
