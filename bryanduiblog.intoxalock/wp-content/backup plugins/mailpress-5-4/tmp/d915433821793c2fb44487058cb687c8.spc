<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<IP>108.200.106.114</IP>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>KS</RegionCode>
	<RegionName>Kansas</RegionName>
	<City>Wichita</City>
	<ZipCode>67219</ZipCode>
	<TimeZone>America/Chicago</TimeZone>
	<Latitude>37.774</Latitude>
	<Longitude>-97.314</Longitude>
	<MetroCode>678</MetroCode>
</Response>
