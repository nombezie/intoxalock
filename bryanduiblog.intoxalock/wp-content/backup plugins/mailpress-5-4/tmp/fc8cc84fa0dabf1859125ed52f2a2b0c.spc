<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Ip>173.51.238.140</Ip>
	<CountryCode>US</CountryCode>
	<CountryName>United States</CountryName>
	<RegionCode>CA</RegionCode>
	<RegionName>California</RegionName>
	<City>La Habra</City>
	<ZipCode>90631</ZipCode>
	<Latitude>33.9414</Latitude>
	<Longitude>-117.9555</Longitude>
	<MetroCode>803</MetroCode>
	<AreaCode>562</AreaCode>
</Response>
