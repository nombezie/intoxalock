<%@ Page Language="C#"  %>
<%
Response.Write("<h1>This is a test page for internal use only.</h1>");

Response.Write("<h2>Server Variables</h2>");
Response.Write("APPL_MD_PATH = " + Request.ServerVariables["APPL_MD_PATH"] + "<br>");
Response.Write("APPL_PHYSICAL_PATH = " + Request.ServerVariables["APPL_PHYSICAL_PATH"] + "<br>");
Response.Write("AUTH_TYPE = " + Request.ServerVariables["AUTH_TYPE"] + "<br>");
Response.Write("AUTH_USER = " + Request.ServerVariables["AUTH_USER"] + "<br>");
Response.Write("AUTH_PASSWORD = " + Request.ServerVariables["AUTH_PASSWORD"] + "<br>");
Response.Write("LOGON_USER = " + Request.ServerVariables["LOGON_USER"] + "<br>");
Response.Write("REMOTE_USER = " + Request.ServerVariables["REMOTE_USER"] + "<br>");
Response.Write("CERT_COOKIE = " + Request.ServerVariables["CERT_COOKIE"] + "<br>");
Response.Write("CERT_FLAGS = " + Request.ServerVariables["CERT_FLAGS"] + "<br>");
Response.Write("CERT_ISSUER = " + Request.ServerVariables["CERT_ISSUER"] + "<br>");
Response.Write("CERT_KEYSIZE = " + Request.ServerVariables["CERT_KEYSIZE"] + "<br>");
Response.Write("CERT_SECRETKEYSIZE = " + Request.ServerVariables["CERT_SECRETKEYSIZE"] + "<br>");
Response.Write("CERT_SERIALNUMBER = " + Request.ServerVariables["CERT_SERIALNUMBER"] + "<br>");
Response.Write("CERT_SERVER_ISSUER = " + Request.ServerVariables["CERT_SERVER_ISSUER"] + "<br>");
Response.Write("CERT_SERVER_SUBJECT = " + Request.ServerVariables["CERT_SERVER_SUBJECT"] + "<br>");
Response.Write("CERT_SUBJECT = " + Request.ServerVariables["CERT_SUBJECT"] + "<br>");
Response.Write("CONTENT_LENGTH = " + Request.ServerVariables["CONTENT_LENGTH"] + "<br>");
Response.Write("CONTENT_TYPE = " + Request.ServerVariables["CONTENT_TYPE"] + "<br>");
Response.Write("GATEWAY_INTERFACE = " + Request.ServerVariables["GATEWAY_INTERFACE"] + "<br>");
Response.Write("HTTPS = " + Request.ServerVariables["HTTPS"] + "<br>");
Response.Write("HTTPS_KEYSIZE = " + Request.ServerVariables["HTTPS_KEYSIZE"] + "<br>");
Response.Write("HTTPS_SECRETKEYSIZE = " + Request.ServerVariables["HTTPS_SECRETKEYSIZE"] + "<br>");
Response.Write("HTTPS_SERVER_ISSUER = " + Request.ServerVariables["HTTPS_SERVER_ISSUER"] + "<br>");
Response.Write("HTTPS_SERVER_SUBJECT = " + Request.ServerVariables["HTTPS_SERVER_SUBJECT"] + "<br>");
Response.Write("INSTANCE_ID = " + Request.ServerVariables["INSTANCE_ID"] + "<br>");
Response.Write("INSTANCE_META_PATH = " + Request.ServerVariables["INSTANCE_META_PATH"] + "<br>");
Response.Write("LOCAL_ADDR = " + Request.ServerVariables["LOCAL_ADDR"] + "<br>");
Response.Write("PATH_INFO = " + Request.ServerVariables["PATH_INFO"] + "<br>");
Response.Write("PATH_TRANSLATED = " + Request.ServerVariables["PATH_TRANSLATED"] + "<br>");
Response.Write("QUERY_STRING = " + Request.ServerVariables["QUERY_STRING"] + "<br>");
Response.Write("REMOTE_ADDR = " + Request.ServerVariables["REMOTE_ADDR"] + "<br>");
Response.Write("REMOTE_HOST = " + Request.ServerVariables["REMOTE_HOST"] + "<br>");
Response.Write("REMOTE_PORT = " + Request.ServerVariables["REMOTE_PORT"] + "<br>");
Response.Write("REQUEST_METHOD = " + Request.ServerVariables["REQUEST_METHOD"] + "<br>");
Response.Write("SCRIPT_NAME = " + Request.ServerVariables["SCRIPT_NAME"] + "<br>");
Response.Write("SERVER_NAME = " + Request.ServerVariables["SERVER_NAME"] + "<br>");
Response.Write("SERVER_PORT = " + Request.ServerVariables["SERVER_PORT"] + "<br>");
Response.Write("SERVER_PORT_SECURE = " + Request.ServerVariables["SERVER_PORT_SECURE"] + "<br>");
Response.Write("SERVER_PROTOCOL = " + Request.ServerVariables["SERVER_PROTOCOL"] + "<br>");
Response.Write("SERVER_SOFTWARE = " + Request.ServerVariables["SERVER_SOFTWARE"] + "<br>");
Response.Write("URL = " + Request.ServerVariables["URL"] + "<br>");
Response.Write("HTTP_CACHE_CONTROL = " + Request.ServerVariables["HTTP_CACHE_CONTROL"] + "<br>");
Response.Write("HTTP_CONNECTION = " + Request.ServerVariables["HTTP_CONNECTION"] + "<br>");
Response.Write("HTTP_VIA = " + Request.ServerVariables["HTTP_VIA"] + "<br>");
Response.Write("HTTP_ACCEPT = " + Request.ServerVariables["HTTP_ACCEPT"] + "<br>");
Response.Write("HTTP_ACCEPT_ENCODING = " + Request.ServerVariables["HTTP_ACCEPT_ENCODING"] + "<br>");
Response.Write("HTTP_ACCEPT_LANGUAGE = " + Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"] + "<br>");
Response.Write("HTTP_COOKIE = " + Request.ServerVariables["HTTP_COOKIE"] + "<br>");
Response.Write("HTTP_HOST = " + Request.ServerVariables["HTTP_HOST"] + "<br>");
Response.Write("HTTP_USER_AGENT = " + Request.ServerVariables["HTTP_USER_AGENT"] + "<br>");
Response.Write("HTTP_UPGRADE_INSECURE_REQUESTS = " + Request.ServerVariables["HTTP_UPGRADE_INSECURE_REQUESTS"] + "<br>");
Response.Write("HTTP_CUDA_CLIIP = " + Request.ServerVariables["HTTP_CUDA_CLIIP"] + "<br>");

System.Web.HttpBrowserCapabilities browser = Request.Browser;
    string s = "<h2>Browser Capabilities</h2>"
        + "Type = "                    + browser.Type + "<br>"
        + "Name = "                    + browser.Browser + "<br>"
        + "Version = "                 + browser.Version + "<br>"
        + "Major Version = "           + browser.MajorVersion + "<br>"
        + "Minor Version = "           + browser.MinorVersion + "<br>"
        + "Platform = "                + browser.Platform + "<br>"
        + "Is Beta = "                 + browser.Beta + "<br />"
        + "Is Crawler = "              + browser.Crawler + "<br>"
        + "Is AOL = "                  + browser.AOL + "<br>"
        + "Is Win16 = "                + browser.Win16 + "<br>"
        + "Is Win32 = "                + browser.Win32 + "<br>"
        + "Supports Frames = "         + browser.Frames + "<br>"
        + "Supports Tables = "         + browser.Tables + "<br>"
        + "Supports Cookies = "        + browser.Cookies + "<br>"
        + "Supports VBScript = "       + browser.VBScript + "<br>"
        + "Supports JavaScript = "     + 
            browser.EcmaScriptVersion.ToString() + "<br />"
        + "Supports Java Applets = "   + browser.JavaApplets + "<br />"
        + "Supports ActiveX Controls = " + browser.ActiveXControls 
              + "<br />"
        + "Supports JavaScript Version = " +
            browser["JavaScriptVersion"] + "<br />";

    Response.Write(s);
%>